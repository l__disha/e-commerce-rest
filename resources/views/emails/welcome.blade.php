@component('mail::message')
# Introduction {{$user->name}}

Thank you for

@component('mail::button', ['url' => route('users.verify',  ['token' => $user->verification_token])])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
