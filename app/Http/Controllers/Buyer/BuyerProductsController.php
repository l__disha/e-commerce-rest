<?php

namespace App\Http\Controllers\Buyer;

use App\Models\Buyer;
use App\Http\Controllers\ApiController;

class BuyerProductsController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index');
    }

    public function index(Buyer $buyer)
    {
        $products = $buyer->transactions()
                            ->with('product')
                            ->get() //collection of transaction with product
                            ->pluck('product');

        return $this->showAll($products);
    }
}
