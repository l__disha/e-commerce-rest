<?php

namespace App\Http\Controllers\Buyer;

use App\Models\Buyer;
use App\Http\Controllers\ApiController;

class BuyersController extends ApiController
{
	public function __construct(){
        $this->middleware('auth:api')->only('index','show');
    }

	public function index()
	{
		// $buyers = Buyer::has('transactions')->get();
        $buyers = Buyer::all();
		return $this->showAll($buyers);
		// return request()->json(['count' => $buyers->count(), 'data' => $buyers], 200);
	}

	public function show($id)
	{
		$buyer = Buyer::findOrFail($id);
        return $this->showOne($buyer);
		// return response()->json(['data' => $buyer], 200);
	}
}
