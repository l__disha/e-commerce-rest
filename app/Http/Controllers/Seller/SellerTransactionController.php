<?php

namespace App\Http\Controllers\Seller;

use App\Models\Seller;
use App\Http\Controllers\ApiController;

class SellerTransactionController extends ApiController
{
    public function __construct(){
        $this->middleware('auth:api')->only('index');
    }

    public function index(Seller $seller)
    {
        $transactions = $seller->products()
                            ->whereHas('transactions')
                            ->with('transactions')
                            ->get()
                            ->pluck('transactions')
                            ->flatten();

        return $this->showAll($transactions);
    }
}
