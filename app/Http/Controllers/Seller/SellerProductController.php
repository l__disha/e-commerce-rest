<?php

namespace App\Http\Controllers\Seller;

use App\Models\Product;
use App\Models\Seller;
use App\Models\User;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\ApiController;
use App\Transformers\ProductTransformer;
use Illuminate\Support\Facades\Storage;

class SellerProductController extends ApiController
{
	public function __construct() {
		$this->middleware('transform.input:' . ProductTransformer::class)->only('store', 'update');
        $this->middleware('auth:api');
	}

	public function index(Seller $seller)
	{
		$products = $seller->products;
		return $this->showAll($products);
	}

	public function store(Request $request, User $seller)
	{
		$rules = [
			'name' => 'required',
			'description' => 'required',
			'quantity' =>  'required|integer|min:1',
			'image' =>  'required|Image'
		];

		$this->validate($request, $rules);
		$data = $request->all();

		$data['status'] = Product::UNAVAILABLE_PRODUCT;
		$data['image'] = $request->image->store('');
		$data['seller_id'] = $seller->id;

		$product = Product::create($data);
		return $this->showOne($product);
	}

	public function update(Request $request, Seller $seller, Product $product)
	{
		$this->verifySeller($seller, $product);

		$rules = [
			'name' => 'min:1',
			'description' => 'min:8',
			'quality' => 'integer|min:3',
			'status' => 'in:' . Product::AVAILABLE_PRODUCT . '.' . Product::UNAVAILABLE_PRODUCT,
			'image' => 'image'
		];

		$this->validate($request, $rules);

		$product->fill(
			$request->only([
				'name',
				'description',
				'quantity'
			])
		);

		if($request->has('status')) {
			$product->status = $request->status;

			if($product->isAvailable() && $product->categories()()->count() == 0) {
				return $this->errorResponse('A produc must be associated with atleast one category t be available', 409);
			}
		}

		if($request->hasFile('image')) {
			Storage::delete($product->image);
			$product->image = $request->image;
		}


		if($product->isClean()) {
			return $this->errorResponse("You have not updated any value", 422);
		}

		$product->save();
		return $this->showOne($product);
	}

	public function destroy(Seller $seller, Product $product)
	{
		$this->verifySeller($seller, $product);
		Storage::delete($product->image);
		$product->delete();
		return $this->showOne($product);
	}

	public function verifySeller(Seller $seller, Product $product)
	{
		if($seller->id != $product->seller_id) {
			throw new HttpException(422, "You are trying to update someone else's product");
		}
	}
}
