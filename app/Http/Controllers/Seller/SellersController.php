<?php

namespace App\Http\Controllers\Seller;

use App\Models\Seller;
use App\Http\Controllers\ApiController;

class SellersController extends ApiController
{
    public function __construct(){
        $this->middleware('auth:api')->only('index','show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::all();
        return $this->showAll($sellers);
        // return response()->json(['count' => $sellers->count(), 'data' => $sellers], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seller = Seller::findOrFail($id);
        return $this->showOne($seller);
        // return response()->json(['data' => $seller], 200);
    }
}
